<?php

namespace TYPO3\CMS\CssStyledContent\TypoScript;

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class CustomConditionFunctionsProvider implements ExpressionFunctionProviderInterface
{
    public function getFunctions()
    {
        return [
            $this->isExtensionLoadedFunction(),
        ];
    }

    protected function isExtensionLoadedFunction(): ExpressionFunction
    {
        return new ExpressionFunction('isExtensionLoaded', function () {
            // Not implemented, we only use the evaluator
        }, function ($existingVariables, $extensionName) {
            return ExtensionManagementUtility::isLoaded($extensionName);
        });
    }
}