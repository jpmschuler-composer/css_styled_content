<?php
return [
    'typoscript' => [
        \TYPO3\CMS\CssStyledContent\ExpressionLanguage\CustomTypoScriptConditionProvider::class,
    ]
];