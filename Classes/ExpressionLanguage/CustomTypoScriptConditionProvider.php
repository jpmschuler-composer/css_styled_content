<?php

namespace TYPO3\CMS\CssStyledContent\ExpressionLanguage;

use TYPO3\CMS\Core\ExpressionLanguage\AbstractProvider;
use TYPO3\CMS\CssStyledContent\TypoScript\CustomConditionFunctionsProvider;

class CustomTypoScriptConditionProvider extends AbstractProvider
{

    public function __construct()
    {
        $this->expressionLanguageProviders = [
            CustomConditionFunctionsProvider::class,
    ];
    }
}